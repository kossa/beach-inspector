<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HotelsApiTest extends TestCase
{
    /**
     * Get Hotels without token.
     *
     * @return void
     */
    public function testGetHotelsWithoutToken()
    {
        $this->get('/api/hotels')
            ->assertStatus(403);
    }

    /**
     * Get Hotels with token.
     *
     * @return void
     */
    public function testGetHotelsWithToken()
    {
        $this->get('/api/hotels', ['APP-TOKEN' => config('app.APP_TOKEN')])
            ->assertStatus(200);
    }
}
