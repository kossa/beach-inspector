
First of all, thank you for the task and the interview, really I learned a lot of thing, for the first time I implanted the TDD, creating vue component, using mix/webpack, I know how to use it, but I never had need to use them.

> **Note**: Here is the online demo: http://beach-inspector.bel4.com/

### Steps  followed:

 1. **TDD**: Creating [HotelsApiTest](https://gitlab.com/kossa/beach-inspector/blob/master/tests/Feature/HotelsApiTest.php#L17) 
 2. Adding [Hotel](https://gitlab.com/kossa/beach-inspector/blob/master/app/Hotel.php) Model and using [Faker](https://github.com/fzaninotto/Faker) to populate the data
 3. Adding Middleware, At this point I was confused, don't know if I'll just need to use simple [Middleware](https://gitlab.com/kossa/beach-inspector/blob/master/app/Http/Middleware/ApiToken.php#L19) for (token based authentication) or I should use [JWT](https://github.com/tymondesigns/jwt-auth), made the login ..etc
 4. Addin Front-end using 3 [vue components](https://vuejs.org/v2/guide/components.html): 
     5. [Hotels](https://gitlab.com/kossa/beach-inspector/blob/master/resources/assets/js/components/hotels/Hotels.vue) (To diplay the list)
     6. [Hotel](https://gitlab.com/kossa/beach-inspector/blob/master/resources/assets/js/components/hotels/Hotel.vue) (Display one hotel)
     7. [Stars](https://gitlab.com/kossa/beach-inspector/blob/master/resources/assets/js/components/commun/Stars.vue) (Display the stars)
 5. Finally I hooked my gitlab repo to server, so when I push automatically my server will receive new updates

