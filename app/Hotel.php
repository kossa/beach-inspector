<?php

namespace App;

use Faker;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    
    /**
     * Get Dummy data
     */
    public function scopeGetDummnyData($q, $rows = 5)
    {
        $faker = Faker\Factory::create();
        $data = [];

        for ($i=0; $i < $rows; $i++) { 
            $data[] = [
                'name'     => $faker->company,
                'distance' => $faker->randomFloat(2, 1, 40),
                'category' => $faker->numberBetween(1, 5),
                'price'    => $faker->numberBetween(10, 200),
            ];
        }

        return collect($data);
    }
}
