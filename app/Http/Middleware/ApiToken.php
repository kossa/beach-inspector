<?php

namespace App\Http\Middleware;

use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check token
        if (is_null($request->header('APP-TOKEN')) || $request->header('APP-TOKEN') != config('app.APP_TOKEN')) {
            return abort(403, 'Token missing.');
        }

        return $next($request);
    }
}
